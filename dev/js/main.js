var $, app;
$ = jQuery;
app = {};

app.init = function() {
	// app.equalize();
	// app.images();
	app.loading();
};

// app.equalize = function() {
// 	var init, layout;
// 	init = function() {
// 		var debounce;
// 		if ($('[data-equalize]').length) {
// 			debounce = false;
// 			$(window).on('resize', function() {
// 				clearTimeout(debounce);
// 				debounce = setTimeout(layout, 250);
// 				return layout();
// 			});
// 			$(window).resize();
// 			return $(window).one('load', function() {
// 				return layout();
// 			});
// 		}
// 	};
// 	layout = function() {
// 		var biggest, biggestHeight, currentHeight, equalizeGroups, group, groupId, item, j, k, key, len, len1, results, toEqualize, usesInner;
// 		toEqualize = $('[data-equalize]');
// 		equalizeGroups = {};
// 		for (j = 0, len = toEqualize.length; j < len; j++) {
// 			item = toEqualize[j];
// 			groupId = $(item).data('equalize');
// 			if (!equalizeGroups[groupId]) {
// 				equalizeGroups[groupId] = [];
// 			}
// 			equalizeGroups[groupId].push(item);
// 		}
// 		results = [];
// 		for (key in equalizeGroups) {
// 			group = equalizeGroups[key];
// 			group = $(group);
// 			if (group.attr('data-equalize-inner')) {
// 				usesInner = true;
// 			} else {
// 				group.height('auto');
// 				usesInner = false;
// 			}
// 			biggest = [];
// 			biggestHeight = 0;
// 			for (k = 0, len1 = group.length; k < len1; k++) {
// 				item = group[k];
// 				if (usesInner) {
// 					currentHeight = $(item).children('.equalize-inner').height();
// 				} else {
// 					currentHeight = $(item).height();
// 				}
// 				if (currentHeight > biggestHeight) {
// 					biggest = $(item);
// 					biggestHeight = currentHeight;
// 				}
// 			}
// 			if (usesInner) {
// 				results.push(group.height($(biggest).children('.equalize-inner').height()));
// 			} else {
// 				results.push(group.height($(biggest).height()));
// 			}
// 		}
// 		return results;
// 	};
// 	return init();
// };
// app.images = function() {
// 	return $('.js-getImage').each(function(i, e) {
// 		var image;
// 		image = $(e).find('.js-image img').attr('src');
// 		$(e).find('.js-image').css('background-image', 'url(' + image + ')');
// 		return $(e).find('.js-image');
// 	});
// };
app.loading = function () {
	return  $('body').imagesLoaded( { background: true }, function() {
		// console.log('imágenes cargadas');
		$('html, body').animate({scrollTop: 0}, 0);
		$('#loading').addClass('js-loaded');
		$('body').removeClass('is-waiting');
		$('body').addClass('js-loaded');
		setTimeout(function(){
			$('#loading').fadeOut(100);
		},100);
		// viewportCheck();
	});
}

// var animationElements = $('[data-animate]');
// function viewportCheck(){
// 	var windowHeight = $(window).height();
// 	var windowTopPosition = $(window).scrollTop();
// 	var windowBottomPosition = (windowTopPosition + windowHeight);
// 	animationElements = $('[data-animate="false"]');

// 	$.each(animationElements, function() {
// 		var element = $(this);
// 		var link = '#'+$(this).attr('id');
// 		var elementHeight = element.outerHeight(true);
// 		var elementTopPosition = element.offset().top;
// 		var elementBottomPosition = (elementTopPosition + elementHeight);
// 		if ((elementBottomPosition >= windowTopPosition) && (elementTopPosition <= windowBottomPosition)) {
// 			element.attr('data-animate', 'true');
// 		}
// 	});
// }

// $(window).on('scroll resize', viewportCheck);

// $(window).trigger('scroll');


$(document).ready(function() {

	var viewport = $(window).height();
	var sizedocument = $(document).height();
	$('.c-step').height(viewport);

	$(window).resize(function(){
		viewport = $(window).height();
		// console.log(viewport);
		$('.c-step').height(viewport);
	});

	$('.c-nav').on('click', '.c-nav__link', function(event) {
		$('[state="1"]').attr('state', '0');
		event.preventDefault();
		if (!$(this).hasClass('is-disabled')) {
			var element = $(this);
			var content = $(this).attr('href');
			toogleMenu(element, content);
		};
	});

	function toogleMenu(element, content) {
		if($(element).attr('state') == 1) {
			// hide
			$(element).attr('state', 0);
		} else {
			// show
			movepage(content);
			$('.c-nav__link').removeClass('is-active');
			$(element).addClass('is-active');
			$(element).attr('state', 1);
		}
	}

	function movepage(content){
		$('.c-step').removeClass('is-active');
		$('body').animate({scrollTop: $(content).offset().top}, 500);
		$(content).addClass('is-active');
	}
	var court = '';
	$('#select_court').on('change', function() {
		court = $(this).find(":selected").val();
	});
	$('.c-step').on('click', '.o-button', function(event) {
		event.preventDefault();
		content = $(this).attr('go');
		if (content  == "#step2") {
			console.log(court);
			if (court == 'racquetball') {
				$('.c-court__field').attr('src', 'img/'+court+'.svg');
				$('.c-players__player:last-child').hide();
				$('.c-players__player:nth-last-child(2)').hide();
				$('.c-summary__player:last-child').hide();
			}else{
				$('.c-court__field').attr('src', 'img/padel.svg');
				$('.c-players__player:last-child').show();
				$('.c-players__player:nth-last-child(2)').show();
				$('.c-summary__player:last-child').show();
			};
		};
		link = $('.c-nav__item').find('.c-nav__link[href="'+content+'"]');
		// console.log(link);
		$('.c-nav__link.is-active').addClass('is-complete').removeClass('is-active');
		link.removeClass('is-disabled').addClass('is-active');
		movepage(content);
	});


	$('.c-form__select').selectpicker({
	style: null
	});



	return app.init();

});


